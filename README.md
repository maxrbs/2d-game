# Robo's Adventures
## Description
### RU
Самый первый проект.
Прототип простой игры жанра Adventure.
Реализовано управление анимированным роботом в стиле платформера. 
Имеется несколько сцен для отображения отдельных локаций – главная карта, карта с разбросанным по ней золотом, локация магазина товаров. 
Реализована простейшая система подбора монет и запись их количества в PlayerPrefs. При покупке товаров количество денег снижается.

### EN
My first project.
A prototype of a simple Adventure game.
Implemented control of an animated robot in the style of a platformer.
There are several scenes for displaying individual locations - the main map, the map with gold on it, the location of the game store.
The simplest system for collecting coins and recording their count in PlayerPrefs has been implemented. When buying goods, the amount of money decreases.

## Screenshots
![Screen1](/Sharing/Screenshots/1.png)
![Screen2](/Sharing/Screenshots/2.png)
![Screen3](/Sharing/Screenshots/3.png)
![Screen4](/Sharing/Screenshots/4.png)
![Screen5](/Sharing/Screenshots/5.png)
![Screen6](/Sharing/Screenshots/6.png)
![Screen7](/Sharing/Screenshots/7.png)

## Links
Build on itch: https://maxrbs.itch.io/robos-adventures