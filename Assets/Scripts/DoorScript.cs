﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DoorScript : MonoBehaviour
{
    Transform icon;

    // Start is called before the first frame update
    void Start()
    {
        icon = transform.GetChild(0);
        icon.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (icon.gameObject.activeSelf && Input.GetKeyDown(KeyCode.E))   
        {
            SceneManager.LoadScene(int.Parse(name));

        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            icon.gameObject.SetActive(true);
        }    
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            icon.gameObject.SetActive(false);
        }
    }

}
