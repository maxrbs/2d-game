﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeadChanger : MonoBehaviour
{
    public Sprite[] Head;
    int i = 0;

    public void OnMouseEnter()
    {
        gameObject.transform.localScale += new Vector3(0.1f, 0.1f, 0f);
        int b = i;
        i = Random.Range(0, Head.Length);

        while (b==i)
            i = Random.Range(0, Head.Length);

        gameObject.GetComponent<Image>().sprite = Head[i];
    }
    
    public void OnMouseExit()
    {
        gameObject.transform.localScale -= new Vector3(0.1f, 0.1f, 0f);
    }

    public void OnMouseDown()
    {
        transform.GetChild(0).gameObject.GetComponent<ParticleSystem>().Play();
    }
}
