﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Buttons : MonoBehaviour
{
    public GameObject menu = null;
    public GameObject settings = null;

    public void StartButton()
    {
        SceneManager.LoadScene("1");
    }

    public void SettingsButton()
    {
        menu.SetActive(false);
        settings.SetActive(true);
    }

    public void MenuButton()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("0");
    }

    public void ExitButton()
    {
        Time.timeScale = 1f;
        Application.Quit();
    }

}
