﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerMove : MonoBehaviour
{
    Animator _animator;
    GameObject _player;
    float Speed = 5f;
    const float Jumpspeed = 0.5f;
    float ShiftSpeed = 2f;
    GameObject _slideShift;
    GameObject _slideJump;


    // Start is called before the first frame update
    void Start()
    {
        GameObject.Find("Text").GetComponent<Text>().text = "Coins: " + PlayerPrefs.GetFloat("coins").ToString();

        _player = GameObject.FindGameObjectWithTag("Player");
        _animator = _player.GetComponent<Animator>();
        _slideShift = GameObject.FindGameObjectWithTag("UI-shift");
        _slideJump = GameObject.FindGameObjectWithTag("UI-jump");

        _animator.SetFloat("jumpspeed", 0f);

        _animator.SetFloat("speed", 0f);
    }

    // Update is called once per frame
    void Update()
    {
        float horz = Input.GetAxis("Horizontal");

        // возврат по нажатии кнопки R
        if (Input.GetKeyDown(KeyCode.R))
            //print(SceneManager.GetActiveScene().name);
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);

        // поворачиваем перса в нужную сторону
        if (horz < 0)
            _player.GetComponent<SpriteRenderer>().flipX = true;
        if (horz > 0)
            _player.GetComponent<SpriteRenderer>().flipX = false;

        /*
        // атака по пробелу
        if (Input.GetKeyDown(KeyCode.Space) && _player.GetComponent<Rigidbody2D>().velocity.y == 0 && !_animator.GetBool("isctrl"))
        {
            if (horz == 0)
                _animator.Play("Attack");
            else
                _animator.Play("Attack2"); 
        }
        */

        // двигаем перса
        _player.transform.Translate(Vector2.right * Speed * ShiftSpeed * horz * Time.deltaTime);
        
        _animator.SetFloat("jumpspeed", _player.GetComponent<Rigidbody2D>().velocity.y);

        _animator.SetFloat("speed", Mathf.Abs(horz));


        // ускорение по шифту
        if (Input.GetKey(KeyCode.LeftShift) && _slideShift.GetComponent<Slider>().value > 0.02f)
            _animator.SetBool("isshift", true);
        else
            _animator.SetBool("isshift", false);

        // создаем множитель скорости
        if (_animator.GetBool("isshift") && _slideShift.GetComponent<Slider>().value > 0.02f)
            ShiftSpeed = 1.7f;
        else
            ShiftSpeed = 1f;

        // убавляем либо заполняем шкалу бега
        if (Input.GetKey(KeyCode.LeftShift) && horz != 0f)
        {
            //_slideShift.GetComponent<Slider>().value = Mathf.Lerp(_slideShift.GetComponent<Slider>().value, 0f, 0.01f);
            _slideShift.GetComponent<Slider>().value -= 0.5f * Time.deltaTime;
        }
        else
        {
            //_slideShift.GetComponent<Slider>().value = Mathf.Lerp(_slideShift.GetComponent<Slider>().value, 1f, 0.01f);
            _slideShift.GetComponent<Slider>().value += 0.1f * Time.deltaTime;
        }

        // приседание по стрелочке вниз
        if (Input.GetKey(KeyCode.DownArrow))
            _animator.SetBool("isctrl", true);
        else
            _animator.SetBool("isctrl", false);


        // прыжок по стрелочке вверх
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            //если в шкале прыжка больше 6/10
            if (_slideJump.GetComponent<Slider>().value > 0.6f)
            {
                _player.GetComponent<Rigidbody2D>().velocity = new Vector2(_player.GetComponent<Rigidbody2D>().velocity.x, 0f);
                _player.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 10) * Jumpspeed, ForceMode2D.Impulse);
                _slideJump.GetComponent<Slider>().value = 0.1f;
            }
        }

        // восстанавливаем шкалу прыжка
        //_slideJump.GetComponent<Slider>().value = Mathf.Lerp(_slideJump.GetComponent<Slider>().value, 1f, 0.035f);        
        _slideJump.GetComponent<Slider>().value += 0.3f * Time.deltaTime;
        _slideJump.GetComponent<Slider>().value = Mathf.Clamp(_slideJump.GetComponent<Slider>().value, 0f, 1f);


        // меню паузы по esc
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PauseOnOff();
        }
    }

    public void PauseOnOff()
    {
        Transform TR = GameObject.Find("Player").transform;
        GameObject UI = null;
        GameObject Pause = null;

        for (int i = 0; i < TR.childCount; i++)
        {
            if (TR.GetChild(i).name == "UI-canvas") UI = TR.GetChild(i).gameObject;
            if (TR.GetChild(i).name == "Pause-canvas") Pause = TR.GetChild(i).gameObject;
        }
        
        if (UI.activeSelf == true) 
        {
            //пауза
            Time.timeScale = 0f;
            UI.SetActive(false);
            Pause.SetActive(true);
        }
        else
        {
            //не пауза
            Time.timeScale = 1f;
            UI.SetActive(true);
            Pause.SetActive(false);
        }
    }
}
