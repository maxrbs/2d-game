﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCMove : MonoBehaviour
{
    int side = 0;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Move());
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector2.left * side * Time.deltaTime * 0.5f);
    }

    IEnumerator Move()
    {
        while (true)
        {
            side = Random.Range(-1, 2);
            if (side == 1)
                GetComponent<SpriteRenderer>().flipX = true;
            else if (side == -1)
                GetComponent<SpriteRenderer>().flipX = false;

            GetComponent<Animator>().SetFloat("speed", Mathf.Abs(side));
            int rand = Random.Range(2, 7);
            yield return new WaitForSeconds(rand);
        }
    }
}
