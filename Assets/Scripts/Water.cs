﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Water : MonoBehaviour
{
    private void OnCollisionStay2D(Collision2D collision)
    {
       if (collision.gameObject.tag == "Player")
       {
            collision.gameObject.GetComponent<Animator>().Play("HitHappens");
            Transform Pl = GameObject.FindGameObjectWithTag("Player").transform;
            Pl.GetChild(0).gameObject.SetActive(true);
       }
        StartCoroutine(Refresh());
    }

    IEnumerator Refresh()
    {
        GameObject.Find("GameController").GetComponent<PlayerMove>().enabled = false;
        yield return new WaitForSeconds(0.8f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        GameObject.Find("GameController").GetComponent<PlayerMove>().enabled = true;
    }

}
