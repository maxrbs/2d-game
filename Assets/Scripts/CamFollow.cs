﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CamFollow : MonoBehaviour
{
    GameObject _player;
    GameObject _camera;
    float quak;
    float _startSize;
    // Start is called before the first frame update
    void Awake()
    {
        _player = GameObject.FindGameObjectWithTag("Player");
        _camera = GameObject.FindGameObjectWithTag("MainCamera");
        _startSize = _camera.GetComponent<Camera>().orthographicSize;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            _camera.GetComponent<Camera>().orthographicSize = Mathf.Lerp(_camera.GetComponent<Camera>().orthographicSize, _startSize + 3f, 0.5f);
        }
        else
        {
            _camera.GetComponent<Camera>().orthographicSize = Mathf.Lerp(_camera.GetComponent<Camera>().orthographicSize, _startSize, 0.7f);
        }

        if (Input.GetAxis("Horizontal") > 0)
            quak = 3.5f;
        else if (Input.GetAxis("Horizontal") < 0)
            quak = -3.5f;
        else
            quak = 0;

        if (Input.GetKey(KeyCode.LeftShift) && GameObject.FindGameObjectWithTag("UI-shift").GetComponent<Slider>().value > 0.02f)
            quak *= 2f;

        _camera.transform.position = Vector3.Lerp(_camera.transform.position, new Vector3(_player.transform.position.x + quak, _player.transform.position.y, -3f), Time.deltaTime * 2f);
    }

}
