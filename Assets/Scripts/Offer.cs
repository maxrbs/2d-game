﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Offer : MonoBehaviour
{
    Transform icon;
    // Start is called before the first frame update
    void Start()
    {
        icon = transform.GetChild(0);
        icon.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (icon.gameObject.activeSelf)
            if (Input.GetKeyDown(KeyCode.E))
            {
                float coins = PlayerPrefs.GetFloat("coins");
                if (coins >= float.Parse(icon.name))
                {
                    PlayerPrefs.SetFloat("coins", coins - float.Parse(icon.name));
                    GameObject.Find("Text").GetComponent<Text>().text = "Coins: " + PlayerPrefs.GetFloat("coins").ToString();
                }
                
            }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            icon.gameObject.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            icon.gameObject.SetActive(false);
        }
    }
}
