﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Coin : MonoBehaviour
{

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
            GetComponent<Animator>().Play("PickUp");

        StartCoroutine(Picked());
    }
    
    IEnumerator Picked()
    {
        yield return new WaitForSeconds(0.9f);
        gameObject.SetActive(false);
        
        //добавить монеты игроку
        float coins = PlayerPrefs.GetFloat("coins");

        PlayerPrefs.SetFloat("coins", coins+1f);
        GameObject.Find("Text").GetComponent<Text>().text = "Coins: " + PlayerPrefs.GetFloat("coins").ToString();
        
    }
}
